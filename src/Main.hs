{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE StandaloneDeriving #-}
-- ----- BEGIN LICENSE BLOCK -----
-- Version: GPL 3.0
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- ----- END LICENSE BLOCK -----

--
-- Copyright (c) 2018, Filip Brcic <brcha@yandex.com>. All rights reserved.
--
-- This file is part of HDimnjak
--

module Main where

import           Control.Applicative     ((<$>), (<*>))
import           Control.Error.Safe      (tryHead)
import           Control.Error.Script    (runScript, scriptIO)
import           Control.Error.Util      (hoistEither)
import           Control.Exception       (throwIO)
import           Control.Monad.IO.Class
import           Data.Default
import           Data.Yaml               (FromJSON (..), Value (..),
                                          decodeHelper, (.!=), (.:), (.:?))
import           Fmt
import           GHC.Generics
import           Numeric.GSL.Integration
import           System.Environment      (getArgs)
import           Text.Libyaml            as Y (decodeFile)


data Parametri = Parametri {
  l1   :: Double, -- Visina donjeg dela dimnjaka [m]
  l2   :: Double, -- Visina gornjeg dela dimnjaka [m]
  l3   :: Double, -- Visina izmedju donjeg dela i sredine dimnjaka [m]
  d1   :: Double, -- Dijametar osnove donjeg dela dimnjaka [m]
  d2   :: Double, -- Dijametar gornjeg dela dimnjaka [m]
  dd1  :: Double, -- Debljina zida u donjem delu [m]
  dd2  :: Double, -- Debljina zida u gornjem delu [m]
  e    :: Double, -- Modul elasticnosti [kPa]
  rho1 :: Double, -- Gustina mase na donjoj polovini [t/m3]
  rho2 :: Double, -- Gustina mase na gornjoj polovini [t/m3]
  m1   :: Double, -- Koncentrisana masa na sredini visine [t]
  m2   :: Double  -- Koncentrisana masa na vrhu dimnjaka [t]
} deriving (Show, Read, Eq)

deriving instance Generic Parametri

instance Default Parametri where
  def = Parametri {
           l1 = 20 -- m
          ,l2 = 40 -- m
          ,l3 = 10 -- m
          ,d1 = 4.3 -- m
          ,d2 = 1.9 -- m
          ,dd1 = 0.007 -- 7 mm
          ,dd2 = 0.006 -- 6 mm
          ,e = 210E6 -- 210 GPa (u kPa)
          ,rho1 = 9.0 -- t/m³
          ,rho2 = 11.0 -- t/m³
          ,m1 = 1.2 -- t
          ,m2 = 1.6 -- t
          }

instance FromJSON Parametri where
  parseJSON (Object v) = Parametri <$> v .:? "l1"   .!= l1 def
                                   <*> v .:? "l2"   .!= l2 def
                                   <*> v .:? "l3"   .!= l3 def
                                   <*> v .:? "d1"   .!= d1 def
                                   <*> v .:? "d2"   .!= d2 def
                                   <*> v .:? "dd1"  .!= dd1 def
                                   <*> v .:? "dd2"  .!= dd2 def
                                   <*> v .:? "e"    .!= e def
                                   <*> v .:? "rho1" .!= rho1 def
                                   <*> v .:? "rho2" .!= rho2 def
                                   <*> v .:? "m1"   .!= m1 def
                                   <*> v .:? "m2"   .!= m2 def
  parseJSON _          = fail "Expecting an object"

decodeFileEither :: FromJSON a => FilePath -> IO (Either String a)
decodeFileEither fp = decodeHelper (Y.decodeFile fp) >>= either throwIO return

ucitajParametre :: FilePath -> IO (Either String Parametri)
ucitajParametre = decodeFileEither

-- Dijametar donjeg dela dimnjaka (kupe) = D1 - x/L1 * (D1-D2)
-- Dijametar gornjeg dela dimnjaka je konstantan (D2)
d :: Parametri -> Double -> Double
d param x = if x <= (l1 param)
            then (d1 param) - ((d1 param) - (d2 param))*x/(l1 param)
            else (d2 param)

-- Gustina mase dimnjaka (veca gore zbog slova)
rho :: Parametri -> Double -> Double
rho param x = if x <= ((l1 param) + (l3 param))
              then (rho1 param)
              else (rho2 param)

-- Debljina zida (dd1 za x < l1, dd2 inace)
dd :: Parametri -> Double -> Double
dd param x = if x <= (l1 param)
             then (dd1 param)
             else (dd2 param)

-- Povrsina preseka
a :: Parametri -> Double -> Double
a param x = pi * (d param x) * (dd param x)

-- Momenat inercije
jz :: Parametri -> Double -> Double
jz param x = (a param x) * ((d param x) ** 2) / 8

-- Psi funkcija
psi :: Parametri -> Double -> Double
psi param x = 1 - cos ( (pi * x) / (2 * l) )
  where l = (l1 param) + (l2 param)

-- Psi sekundum funkcija
psi_sec :: Parametri -> Double -> Double
psi_sec param x = (pi / (2*l)) ** 2 * cos ( (pi * x) / (2 * l) )
  where l = (l1 param) + (l2 param)

-- Masa dimnjaka po jedinici visine u funkciji x
m :: Parametri -> Double -> Double
m param x = (rho param x) * (a param x)

-- Integral funkcije
integral :: (Double -> Double) -> Double -> Double -> (Double, Double)
integral = integrateQAGS 1E-9 1000

-- Ekvivalentna masa SDOF modela
m_ekvivalentna :: Parametri -> (Double, Double)
m_ekvivalentna param = integral funkcija 0 l
  where
    l = (l1 param) + (l2 param)
    funkcija = (\x -> (m param x) * ((psi param x) ** 2))

-- Ekvivalentna krutost SDOF modela
k_ekvivalentna :: Parametri -> (Double, Double)
k_ekvivalentna param = integral funkcija 0 l
  where
    l = (l1 param) + (l2 param)
    funkcija = (\x -> (e param) * (jz param x) * ((psi_sec param x) ** 2))

-- Integrali sve -- (m_ekv, m_ekv_err, k_ekv, k_ekv_err)
integrali_sve :: Parametri -> (Double, Double, Double, Double)
integrali_sve param = (m_ekv, m_ekv_err, k_ekv, k_ekv_err)
  where
    (m_ekv, m_ekv_err) = m_ekvivalentna param
    (k_ekv, k_ekv_err) = k_ekvivalentna param

m_zvezdica :: Parametri -> Double -> Double
m_zvezdica param m_ekv = m_ekv + suma
    where
      suma = (m1 param) * psi2 + (m2 param)
      l1l3 = (l1 param) + (l3 param)
      psi2 = (psi param l1l3) ** 2

main :: IO ()
main = runScript $ do
  args      <- scriptIO getArgs
  paramFile <- tryHead "Navedite fajl sa parametrima" args
  param'    <- scriptIO $ ucitajParametre paramFile
  param     <- case param' of
    Left msg -> liftIO (putStrLn msg) >> return def
    Right p  -> return p

  scriptIO $ do
    let (m_ekv, m_ekv_err, k_ekv, k_ekv_err) = integrali_sve param
    let m_z = m_zvezdica param m_ekv
    let omega_z = sqrt ( k_ekv / m_z )
    let f_z = omega_z / (2*pi)
    let t_z = 1 / f_z
    fmtLn $ "\nREZULTATI proracuna za ulazne parametre:"
    fmtLn $ "==========================================\n"
    fmtLn $ "gustina mase rho1 (donja polovina dimnjaka):  "+|rho1 param|+" t/m\n"
    fmtLn $ "gustina mase rho2 (gornja polovina dimnjaka): "+|rho2 param|+" t/m\n"
    fmtLn $ "koncentrisana masa M1 (platforma na polvini): "+|m1 param|+" t\n"
    fmtLn $ "koncentrisana masa M2 (platforma na vrhu):    "+|m2 param|+" t\n"
    fmtLn $ "\nEkvivalentna masa (itegral) je "+|m_ekv|+" ± "+|m_ekv_err|+" t \n"
    fmtLn $ "Koncentrisana masa (platforme) "+|m_z - m_ekv|+" t\n"
    fmtLn $ "Ukupna Ekvivalentna masa je    "+|m_z|+" t\n"
    fmtLn $ "Ekvivalentna krutost (integral) je "+|k_ekv|+" ± "+|k_ekv_err|+" kN/m\n"
    fmtLn $ "Kruzna frekvencija je "+|omega_z|+" rad/s\n"
    fmtLn $ "Frekvecija (u Hz) je  "+|f_z|+" Hz\n"
    fmtLn $ "Period (u sec) je     "+|t_z|+" s\n"
